package com.ey.Products.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ey.Products.entity.Product;
import com.ey.Products.repository.ProductRepository;

@Service
public class ProductServiceImplementation implements ProductService{

	@Autowired
	ProductRepository productRepository;
	@Override
	public List<Product> getAllProducts() {
		// TODO Auto-generated method stub
		return productRepository.findAll();
	}

	@Override
	public void addProducts(Product product) {
		// TODO Auto-generated method stub
		productRepository.save(product);
	}

	@Override
	public Product getProductById(Long productId) {
		// TODO Auto-generated method stub
		return productRepository.findById(productId).orElse(null);
	}

	@Override
	public void deleteProductById(Long productId) {
		// TODO Auto-generated method stub
		productRepository.deleteById(productId);
	}

//	@Override
//	public String updateProductById(Product product) {
//		// TODO Auto-generated method stub
//		Product prod= productRepository.findById(product.getProductId()).orElse(null);
//		prod.setProductName(product.getProductName());
//		prod.setProductDescription(product.getProductDescription());
//		prod.setProductCost(product.getProductCost());
//		prod.setProductQuantity(product.getProductQuantity());
//		
//		return "updated";
//	}

	

}
