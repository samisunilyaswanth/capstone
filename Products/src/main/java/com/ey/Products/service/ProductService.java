package com.ey.Products.service;

import java.util.List;

import com.ey.Products.entity.Product;


public interface ProductService {
	public List<Product> getAllProducts();
	public void addProducts(Product product);
	public Product getProductById(Long productId);
	public void deleteProductById(Long productId);
//	public String updateProductById(Product product);
}
