package com.ey.ums.controller;

import java.security.Principal;  
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import com.ey.ums.global.GlobalList;
import com.ey.ums.model.Cart;
import com.ey.ums.model.CheckOut;
import com.ey.ums.model.Product;
import com.ey.ums.model.UserDtls;
import com.ey.ums.repository.CartRepository;
import com.ey.ums.repository.ProductRepository;
import com.ey.ums.repository.UserRepository;
import com.ey.ums.repository.ViewRepository;
import com.ey.ums.service.CartService;
import com.ey.ums.service.CheckOutService;
import com.ey.ums.service.UserService;



@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserRepository userRepo;
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	UserService service;
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	CartRepository cartRepository;
	
	@Autowired
	CartService cartService;
	
	
	@Autowired
	CheckOutService checkOutService;
	
	
	@ModelAttribute
	private void userDetails(Model m, Principal p) {
		String email = p.getName();
		UserDtls user = userRepo.findByEmail(email);

		m.addAttribute("user", user);

	}

//	@GetMapping("/")
//	public String home() {
//		return "/home";
//	}
	@GetMapping("/")
	public String getAllProducts(Model model){
	
		List<Product> list = restTemplate.getForObject("http://localhost:9001/product/show", List.class);
		System.out.println("-----------------------------------------------=============================================hELLO");
		model.addAttribute("products",list);
		System.out.println( model.getAttribute("products"));
		
		return "index1"; 
		
	}
	
//	@GetMapping("/UpdateUserForm/{id}")
//	public String updateUserForm(@PathVariable Integer id,Model model)
//	{
//		UserDtls dtls = service.getUserById(id);
//		model.addAttribute("UpdateUserObj", dtls);
//		return "adminn/NewUpdateUserForm";
//	}
	
	@GetMapping("/getCart/{id}")
	public String cartPage(@PathVariable Long id,Model model) {
		Product product = service.getById(id);
		
		model.addAttribute("cart", product);
		return "cartPage";
	}


@GetMapping("/addToCartById/{id}")
public String addToCart(@PathVariable Long id,Model model) {
    Product product= productRepository.findById(id).get();
    
    Cart newCart = new Cart();
    newCart.setOrderId((long) 1);
    newCart.setProduct(product);
    System.out.println(newCart.getProduct());
    cartRepository.save(newCart);
    model.addAttribute("products", cartService.getAllCartItems());
    model.addAttribute("total", cartService.getSum());
    return "addToCartPage";
}

@GetMapping("/addTocart")
public String redirectToCart(Model model) {
	model.addAttribute("products", cartService.getAllCartItems());
	
	 model.addAttribute("total", cartService.getSum());
	return "addToCartPage";
}


	@GetMapping("/deleteUserOrder/{orderid}")
	public String deleteUserOrder(@PathVariable Long orderid) {
		cartService.deleteOrder(orderid);
		
		return "redirect:/user/addTocart";
	}
	
	@GetMapping("/clearCart")
	public String clearCart() {
		cartService.deleteCart();
		return "clearCartPage";
	}
	
	
	//Not Required
	@GetMapping("/billGenerate")
	public String billGenerate(Model model) {
		model.addAttribute("total", cartService.getSum());
		CheckOut checkOut = new CheckOut();
		model.addAttribute("checkOut", checkOut);
		return "checkOut";
	}

	
	@PostMapping("/checkOutBill")
	public String checkOutBill(@ModelAttribute CheckOut checkOut) {
		checkOutService.saveBill(checkOut);
		return "Finish";
	}
	
}
