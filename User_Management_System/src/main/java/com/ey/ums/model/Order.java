package com.ey.ums.model;


import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "orders")
public class Order {

	@Id
	private int cvv;
	private String name;
	private String address;
	private Long cardNumber;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Long getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
	}
	public int getCvv() {
		return cvv;
	}
	public void setCvv(int cvv) {
		this.cvv = cvv;
	}
	public Order( String name, String address, Long cardNumber, int cvv) {
		super();
		
		this.name = name;
		this.address = address;
		this.cardNumber = cardNumber;
		this.cvv = cvv;
	}
	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}

}
