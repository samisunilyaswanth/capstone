package com.ey.ums.service;

import com.ey.ums.model.Order;

public interface OrderService {

	void saveOrder(Order order);
	
}
